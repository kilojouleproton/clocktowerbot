# Setup

1. Install discord.py somewhere in your $PYTHONPATAH

2. [Create](https://discord.com/developers/applications) a Discord application

3. Create a bot account for your ClocktowerBot application

4. Set the token field in your config to your bot account's token. It will be in <https://discord.com/developers/applications/CLIENT_ID_HERE/bot>

5. Run the bot on your config file

```bash
python clocktower.py example.cfg
```
