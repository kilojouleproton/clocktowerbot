#!/usr/bin/env python

import argparse
import configparser
import asyncio
import discord
import shlex
import re
import time
import random
import clockplot
import tempfile
import uuid

class NoGamesError(IndexError): pass
class AmbiguousGameError(IndexError): pass
class NotPlayerError(IndexError): pass
class OmaeWaMouShindeiruError(IndexError): pass


def death_message(user):
    if wagonmode and user.bot: return '**{} was scuttled!**'.format(user.mention)
    elif wagonmode and random.random() < 0.25: return '**{} was scuttled!**'.format(user.mention)

    return random.choice([
        '**{} was killed!**'.format(user.mention),
        '**{} was struck down!**'.format(user.mention),
        '**{} is dead!**'.format(user.mention),
        '**{} has died!**'.format(user.mention),
        '**{} dies!**'.format(user.mention),
        '**{} has passed on!**'.format(user.mention),
        '**{} has expired!**'.format(user.mention),
        '**{} has ceased to be!**'.format(user.mention),
        '**{} has gone to meet their maker!**'.format(user.mention),
        '**Bereft of life, {} rests in peace!**'.format(user.mention),
        '**{} is pushing up the daisies!**'.format(user.mention),
        '**{}\'s metabolic processes are now history!**'.format(user.mention),
        '**{} is off the twig!**'.format(user.mention),
        '**{}\'s shuffled off their mortal coil!**'.format(user.mention),
        '**{} has run down the curtain!**'.format(user.mention),
        '**{} has joined the choir invisible!**'.format(user.mention),
        '**{} kicked the bucket!**'.format(user.mention),
        '**{} is no more!**'.format(user.mention),
    ])

def get_game(games, text):
    return games[get_gameid(games, text)]

def get_gameid(games, text):
    if not games: raise NoGamesError
    elif len(games) == 1: return 0
    else:
        gameid = re.findall('#([0-9]+)', text.strip())
        if not gameid: raise AmbiguousGameError
        return int(gameid[0]) - 1


def fuzzy_time(start):
    interval = time.time() - start
    if interval < 60*1: return 'less than a minute ago'
    elif interval < 60*2: return 'about a minute ago'
    elif interval < 60*8: return 'about five minutes ago'
    elif interval < 60*12: return 'about ten minutes ago'
    elif interval < 60*17: return 'about fifteen minutes ago'
    elif interval < 60*24: return 'about twenty minutes ago'
    elif interval < 60*36: return 'about thirty minutes ago'
    elif interval < 60*50: return 'less than an hour ago'
    elif interval < 3600*1.5: return 'about an hour ago'
    elif interval < 3600*2.5: return 'about two hours ago'
    elif interval < 3600*8: return 'a few hours ago'
    elif interval < 3600*16: return 'half a day ago'
    elif interval < 3600*32: return 'about a day ago'
    elif interval < 3600*56: return 'about two days ago'
    elif interval < 86400*4: return 'a few days ago'
    elif interval < 86400*11: return 'about a week ago'
    elif interval < 86400*30: return 'a few weeks ago'
    else: return 'a forever ago'

def grep_mentions(text): return [int(x) for x in re.findall('<@!?([0-9]+)>', text)]

class Game(object):
    storyteller = None
    players = None

    dead = None
    deadvotes = None

    start = 0
    def __init__(self, *players, storyteller=None):
        self.players = players
        self.storyteller = storyteller
        self.start = time.time()

        self._ids = set([player.id for player in self.players])

        self.dead = [False for player in self.players]
        self.deadvotes = [1 for player in self.players]

    def get_player_mentions(self):
        return ' '.join([player.mention for player in self.players])

    def _plist(self):
        names = []
        for player, dead in zip(self.players, self.dead):
            if dead: names.append('~~{}~~'.format(player.display_name))
            else: names.append('**{}**'.format(player.display_name))
        return ', '.join(names)

    def get_pretty_list(self):
        timestr = fuzzy_time(self.start)    
        out = 'Game started by {} {} with {} living and {} dead: {}'.format(self.storyteller.display_name, timestr, len(self.players) - sum(self.dead), sum(self.dead), self._plist())
        return out

    def get_voters(self, mention=True):
        names = []
        for player, dead, deadvote in zip(self.players, self.dead, self.deadvotes):
            if mention: pstr = player.mention
            else: pstr = player.display_name
            if dead:
                if deadvote: pstr += ' (dead, can vote)'
                else: pstr += ' (dead, no vote)'
            names.append(pstr)
        return ', '.join(names)

    def get_player_id(self, target):
        for i, player in enumerate(self.players):
            if player == target: return i
        raise IndexError

    def get_vote_order(self, target):
        targetid = self.get_player_id(target)
        voters = []
        for i in range(len(self.players)):
            voters.append(self.players[targetid-i-1])
        return voters

    def is_dead(self, target):
        return self.dead[self.get_player_id(target)]

    def can_vote(self, target):
        if not self.is_dead(target): return True
        elif self.deadvotes[self.get_player_id(target)]: return True
        else: return False

    def kill(self, user):
        for i, player in enumerate(self.players):
            if self.dead[i]: pass #raise OmaeWaMouShindeiruError
            if player == user:
                self.dead[i] = True
                return True
        raise NotPlayerError

    def unkill(self, text):
        count = 0
        for userid in grep_mentions(text):
            for i, u in enumerate(self.players):
                if u.id == userid:
                    self.dead[i] = False
                    count += 1
        return count


    def restore_deadvote(self, userids):
        count = 0
        for i, player in enumerate(self.players):
            if player.id in userids: 
                self.deadvotes[i] = True
                count += 1
        return count

    def remove_deadvote(self, userids):
        count = 0
        for i, player in enumerate(self.players):
            if player.id in userids: 
                self.deadvotes[i] = False
                count += 1
        return count

    def process_votes(self, evlist, yes, no):
        votes = {}
        for player in self.players: votes[player.id] = False

        for reaction, user in evlist:
            if user.id not in self._ids: continue

            if reaction.emoji == yes: votes[user.id] = True
            if reaction.emoji == no: votes[user.id] = False

        support = 0
        oppose = 0
        living = 0
        summary = []
        for i, (player, dead, deadvote) in enumerate(zip(self.players, self.dead, self.deadvotes)):
            if dead:
                if deadvote:
                    if votes[player.id]:
                        support += 1
                        self.deadvotes[i] = False
                        summary.append('{} (dead) used a deadvote to vote **Yea**'.format(player.display_name))
                    else: 
                        oppose += 1
                        summary.append('{} (dead) didn\'t use a deadvote, voting **Nay**'.format(player.display_name))
                else:
                    oppose += 1
                    summary.append('{} (dead) has no deadvote, voting **Nay**'.format(player.display_name))
            else:
                living += 1
                if votes[player.id]: 
                    support += 1
                    summary.append('{} voted **Yea**'.format(player.display_name))
                else: 
                    oppose += 1
                    summary.append('{} voted **Nay**'.format(player.display_name))

        summary = '\n'.join(summary)

        #TODO: tie break?
        if support >= (living / 2): return True, summary, support
        else: return False, summary, support

    @staticmethod
    async def from_string(client, author, text, gamelist):
        try:
            players = []
            added = set()
            for userid in grep_mentions(text):
                if userid in added: continue
                players.append(await client.fetch_user(userid))
                added.add(userid)
            #random.shuffle(players)
            gamelist.append(Game(*players, storyteller=author))
            return 0
        finally: return 1

def load_config(fh):
    cfgparser = configparser.ConfigParser()
    cfgparser.read(fh)
    return cfgparser

def get_usagestr(prefix):
    out = '**General commands: (Prefix is {prefix})**\n'.format(prefix=prefix)
    out += '{prefix}game: Start a new game as the Storyteller with the mentioned players\n'.format(prefix=prefix)
    out += '{prefix}listgame, {prefix}lsgame, or {prefix}gamelist: List all running games\n'.format(prefix=prefix)
    out += '{prefix}listplayers, {prefix}lsplayers, or {prefix}players: List players for a running game\n'.format(prefix=prefix)
    out += '**Storyteller-only commands:**\n'.format(prefix=prefix)
    out += '{prefix}kill: Kill the mentioned player(s). If running several games at once, specify which game to operate on.\n'.format(prefix=prefix)
    out += '{prefix}unkill: Bring back the mentioned player(s). If running several games at once, specify which game to operate on.\n'.format(prefix=prefix)
    out += '{prefix}addvote: Restore a player\'s deadvote\n'.format(prefix=prefix)
    out += '{prefix}rmvote: Remove a player\'s deadvote\n'.format(prefix=prefix)
    out += '{prefix}asyncvote: Start an asynchronous (or simultaneous) vote for someone\'s execution. Optionally specify a vote duration. Does *not* automatically kill the target. Optionally accepts a time in seconds\n'.format(prefix=prefix)
    out += '{prefix}singlevote: Start a synchronous (or sequential) vote for someone\'s execution placed on a single message (which introduces lots of nasty, hard-to-debug bugs). Optionally specify a vote duration. Does *not* automatically kill the target. Optionally accepts a time in seconds\n'.format(prefix=prefix)
    out += '{prefix}vote: Start a synchronous (or sequential) vote for someone\'s execution placed on separate messages. Optionally specify a vote duration. Does *not* automatically kill the target. Optionally accepts a time in seconds\n'.format(prefix=prefix)
    out += '\n'
    out += '**Examples:**\n'
    out += '{prefix}game @Player1 @Player2 @Player3 -- Start a new game with Player1, Player2, and Player3\n'.format(prefix=prefix)
    out += '{prefix}vote @Player2 30s -- Vote to kill Player2 for the next 30 seconds\n'.format(prefix=prefix)
    out += '{prefix}addvote @Player3 -- Restore Player3\'s deadvote\n'.format(prefix=prefix)
    out += '{prefix}kill @Player2 -- Storyteller kills Player2\n'.format(prefix=prefix)
    out += '{prefix}end -- End the game'.format(prefix=prefix)

    return out

wagonmode = True

def main(config):
    token = config.get('DEFAULT', 'token', fallback=None)
    if token is None: raise ValueError('Invalid token: {}'.format(token))

    allowed_channels = shlex.split(config.get('DEFAULT', 'allowed_channels', fallback=''))

    prefix = config.get('DEFAULT', 'prefix', fallback='/')

    defaultvotetime = config.getint('DEFAULT', 'votetime', fallback=30)

    yesemoji = config.get('DEFAULT', 'yes', fallback='👍')
    noemoji = config.get('DEFAULT', 'no', fallback='👎')

    client = discord.Client()

    games = []

    usagestr = get_usagestr(prefix=prefix)

    @client.event
    async def on_message(message):
        if allowed_channels and (message.channel.name not in allowed_channels): return
        elif not message.content.startswith(prefix): return
        elif message.author == client.user: return

        noprefix = message.content[len(prefix):].lower()
        if False: pass

        ### HELP ###
        elif noprefix.startswith('help'): await message.channel.send(usagestr)

        ### LISTGAMES ###
        elif noprefix.startswith('listgame') or noprefix.startswith('gamelist') or noprefix.startswith('lsgame'):
            if games:
                out = ''
                for gameid, game in enumerate(games): out += 'Game #{}: {}'.format(gameid+1, game.get_pretty_list() + '\n')
                await message.channel.send(out)
            else:
                await message.channel.send('No games are running right now.')

        ### LISTPLAYERS ###
        elif noprefix.startswith('listplayer') or noprefix.startswith('player') or noprefix.startswith('lsplayer'):
            try: game = get_game(games, message.content)

            except NoGamesError:
                await message.channel.send('No games are running right now. There is nothing to end.')
                return
            except AmbiguousGameError:
                await message.channel.send('Please specify a game. Use /list to get a list of running games.\nUsage: /listplayer PLAYER(S) #GAMEID')
                return

            tf = tempfile.NamedTemporaryFile(suffix='.png')
            clockplot.clockify(game, tf.name)
            tf.flush()
            with open(tf.name, 'rb') as fh:
                imfile = discord.File(fh, '{}.png'.format(uuid.uuid4()))

            await message.channel.send('Storyteller: {}\nPlayers: {}'.format(game.storyteller, game.get_voters(mention=False)), file=imfile)

        ### GAME ###
        elif noprefix.startswith('game'):    
            await Game.from_string(client, message.author, message.content[4:].strip(), games)
            await message.channel.send('Started a new game run by {} in {}'.format(message.author.mention, message.channel.mention))

        ### KILL ###
        elif noprefix.startswith('kill'):
            try: game = get_game(games, message.content)

            except NoGamesError:
                await message.channel.send('No games are running right now. There is nothing to end.')
                return
            except AmbiguousGameError:
                await message.channel.send('Please specify a game. Use /list to get a list of running games.\nUsage: /kill PLAYER(S) #GAMEID')
                return
            
            if message.author != game.storyteller: 
                await message.channel.send('Error: You are not the Storyteller for this game!')
                return

            count = 0
            for userid in grep_mentions(message.content):
                target = await client.fetch_user(userid)
                try: 
                    game.kill(target)
                    await message.channel.send(death_message(target))
                    count += 1
                except OmaeWaMouShindeiruError:
                    await message.channel.send('Error: {} is already dead!'.format(user.display_name))
                except NotPlayerError:
                    await message.channel.send('Error: {} is not a player'.format(user.display_name))
            if count == 0: await message.channel.send('Failed to kill anyone')

        ### UNKILL ###
        elif noprefix.startswith('unkill'):
            try: game = get_game(games, message.content)

            except NoGamesError:
                await message.channel.send('No games are running right now. There is nothing to end.')
                return
            except AmbiguousGameError:
                await message.channel.send('Please specify a game. Use /list to get a list of running games.\nUsage: /unkill PLAYER(S) #GAMEID')
                return
            
            if message.author != game.storyteller: 
                await message.channel.send('Error: You are not the Storyteller for this game!')
                return

            count = game.unkill(message.content)
            if count == 0: await message.channel.send('Failed to unkill anyone')
            await message.channel.send('Successfully unkilled {} player'.format(count))

        ### END ###
        elif noprefix.startswith('end'):
            try: game = get_game(games, message.content)
            except NoGamesError:
                await message.channel.send('No games are running right now. There is nothing to end.')
                return
            except AmbiguousGameError:
                await message.channel.send('Please specify a game. Use /list to get a list of running games.\nUsage: /end PLAYER(S) #GAMEID')
                return
            
            if message.author != game.storyteller: 
                await message.channel.send('Error: You are not the Storyteller for this game!')
                return

            games.remove(game)
            del game
            await message.channel.send('Successfully ended a game')

        ### VOTE ###
        elif noprefix.startswith('vote'):
            try: 
                gameid = get_gameid(games, message.content)
                game = games[gameid]
            except NoGamesError:
                await message.channel.send('No games are running right now.')
                return
            except AmbiguousGameError:
                await message.channel.send('Please specify a game. Use /list to get a list of running games.\nUsage: /asyncvote PLAYER #GAMEID TIMEs')
                return
            
            if message.author != game.storyteller: 
                await message.channel.send('Error: You are not the Storyteller for this game!')
                return

            userids = grep_mentions(message.content)
            if len(userids) == 1:
                target = await client.fetch_user(userids[0])
            else:
                await message.channel.send('Error: Please specify one player')
                return

            found = False
            for player in game.players:
                if player.id == target.id: found = True
            if not found:
                await message.channel.send('Error: {} is not a player'.format(target.display_name))
                return

            if re.search(' [0-9\.]+s', message.content):
                votetime = float(re.findall(' ([0-9]+)s', message.content)[0])
            else: votetime = defaultvotetime

            voteorder = game.get_vote_order(target)

            await message.channel.send('Execute {}?'.format(target.mention))

            evlist = []
            voted = []
            support = 0
            oppose = 0
            for voterid, player in enumerate(voteorder):
                prompt = '{}, do you vote to execute?'.format(player.mention)
                timeleft = '**{}s left**'.format(int(round(votetime)))
                promptmessage = await message.channel.send('{} {}'.format(prompt, timeleft))
                await promptmessage.add_reaction(yesemoji)
                await promptmessage.add_reaction(noemoji)
                voted.append('')

                start = time.time()
                while time.time() < (start + votetime):
                    try:
                        reaction, user = await client.wait_for('reaction_add', timeout=0.8)
                        if user == player:
                            if not ((reaction.emoji == yesemoji) or (reaction.emoji == noemoji)): continue
                            if reaction.emoji == yesemoji: voted[-1] = '{} voted **Yea**'.format(player.display_name)
                            elif reaction.emoji == noemoji: voted[-1] = '{} voted **Nay**'.format(player.display_name)
                            evlist.append((reaction, user))
                            await promptmessage.edit(content='{} {}'.format(prompt, voted[-1]))
                            break
                    except asyncio.TimeoutError: pass
                    timeleft = '**{}s left**'.format(int(round(votetime - (time.time() - start))))
                    await promptmessage.edit(content='{} {}'.format(prompt, timeleft))
                if not voted[-1]: 
                    voted[-1] = '{} voted **Nay**'.format(player.display_name)
                    await promptmessage.edit(content='{} {}'.format(prompt, voted[-1]))

            outcome, summary, support = game.process_votes(evlist, yes=yesemoji, no=noemoji)


            #if outcome: await message.channel.send('Results for game #{}\n{}\nResult: **Execute** {} ({} votes for)'.format(gameid+1, '\n'.join(voted) + '\n', target.mention, support))
            #else: await message.channel.send('Results for game #{}\n{}\nResult: **Spare** {}'.format(gameid+1, summary, target.mention))
            if outcome: await message.channel.send('Results for game #{}\n{}\nResult: **Execute** {} ({} votes for)'.format(gameid+1, summary + '\n', target.mention, support))
            else: await message.channel.send('Results for game #{}\n{}\nResult: **Spare** {}'.format(gameid+1, summary, target.mention))

        ### SINGLEVOTE ###
        elif noprefix.startswith('singlevote'):
            try: 
                gameid = get_gameid(games, message.content)
                game = games[gameid]
            except NoGamesError:
                await message.channel.send('No games are running right now.')
                return
            except AmbiguousGameError:
                await message.channel.send('Please specify a game. Use /list to get a list of running games.\nUsage: /asyncvote PLAYER #GAMEID TIMEs')
                return
            
            if message.author != game.storyteller: 
                await message.channel.send('Error: You are not the Storyteller for this game!')
                return

            userids = grep_mentions(message.content)
            if len(userids) == 1:
                target = await client.fetch_user(userids[0])
            else:
                await message.channel.send('Error: Please specify one player')
                return

            found = False
            for player in game.players:
                if player.id == target.id: found = True
            if not found:
                await message.channel.send('Error: {} is not a player'.format(target.display_name))
                return

            if re.search(' [0-9\.]+s', message.content):
                votetime = float(re.findall(' ([0-9]+)s', message.content)[0])
            else: votetime = defaultvotetime

            voteorder = game.get_vote_order(target)

            prompt = 'Execute {}? (Vote additions will only be counted during your voting window; if this message didn\'t update with your vote, it wasn\'t counted!)\n'.format(target.mention, yesemoji, noemoji)
            timeleft = ' **{}s left**\n'.format(int(round(votetime)))
            voters = game.get_voters()
            votes = ''
            promptmessage = await message.channel.send(prompt + timeleft)
            await promptmessage.add_reaction(yesemoji)
            await promptmessage.add_reaction(noemoji)

            voted = []
            evlist = []
            for voterid, player in enumerate(voteorder):
                voter = '{}, you\'re up!\n'.format(player.mention)
                await promptmessage.edit(content=(prompt + '\n'.join(voted) + '\n' + voter + timeleft))
                
                if not game.can_vote(player): 
                    voted.append('{} voted **Nay**'.format(player.display_name))
                    evlist.append((discord.Reaction(message=promptmessage, data={}, emoji=noemoji), player))
                    continue
                voted.append('')
                start = time.time()
                while time.time() < (start + votetime):
                    timeleft = ' **{}s left**\n'.format(int(round(votetime - (time.time() - start))))
                    await promptmessage.edit(content=(prompt + '\n'.join(voted) + '\n' + voter + timeleft))
                    try: 
                        reaction, user = await client.wait_for('reaction_add', timeout=0.8)
                        if user == player: 
                            if not ((reaction.emoji == yesemoji) or (reaction.emoji == noemoji)): continue
                            evlist.append((reaction, user))
                            if reaction.emoji == yesemoji: voted[-1] = '{} voted **Yea**'.format(player.display_name)
                            if reaction.emoji == noemoji: voted[-1] = '{} voted **Nay**'.format(player.display_name)
                            await promptmessage.edit(content=(prompt + '\n'.join(voted) + '\n' + voter + timeleft))
                            break
                    except asyncio.TimeoutError: pass
                if not voted[-1]: voted[-1] = '{} voted **Nay**'.format(player.display_name)

            timeleft = 'Voting is up!\n'
            #voted[-1] = '{} voted **Nay**'.format(player.display_name)
            #evlist.append((discord.Reaction(message=promptmessage, data={}, emoji=noemoji), player))
            await promptmessage.edit(content=(prompt + '\n'.join(voted) + '\n' + timeleft))

            outcome, summary, support = game.process_votes(evlist, yes=yesemoji, no=noemoji)
            
            if outcome: await message.channel.send('Results for game #{}\n{}\nResult: **Execute** {} ({} votes for)'.format(gameid+1, '\n'.join(voted) + '\n', target.mention, support))
            else: await message.channel.send('Results for game #{}\n{}\nResult: **Spare** {}'.format(gameid+1, summary, target.mention))

        ### ASYNCVOTE ###
        elif noprefix.startswith('asyncvote'):
            try: 
                gameid = get_gameid(games, message.content)
                game = games[gameid]
            except NoGamesError:
                await message.channel.send('No games are running right now.')
                return
            except AmbiguousGameError:
                await message.channel.send('Please specify a game. Use /list to get a list of running games.\nUsage: /asyncvote PLAYER #GAMEID TIMEs')
                return
            
            if message.author != game.storyteller: 
                await message.channel.send('Error: You are not the Storyteller for this game!')
                return

            userids = grep_mentions(message.content)
            if len(userids) == 1:
                target = await client.fetch_user(userids[0])
            else:
                await message.channel.send('Error: Please specify one player')
                return

            found = False
            for player in game.players:
                if player.id == target.id: found = True
            if not found:
                await message.channel.send('Error: {} is not a player'.format(target.display_name))
                return

            if re.search(' [0-9\.]+s', message.content):
                votetime = float(re.findall(' ([0-9]+)s', message.content)[0])
            else: votetime = defaultvotetime

            prompt = 'Execute {}? {}/{}'.format(target.mention, yesemoji, noemoji)
            timeleft = ', {}s left\n'.format(int(round(votetime)))
            voters = game.get_voters()
            promptmessage = await message.channel.send(prompt + timeleft + voters)
            await promptmessage.add_reaction(yesemoji)
            await promptmessage.add_reaction(noemoji)
            start = time.time()

            evlist = []
            while time.time() < (start + votetime):
                timeleft = ', {}s left\n'.format(int(round(votetime - (time.time() - start))))
                await promptmessage.edit(content=(prompt + timeleft + voters))
                try: 
                    reaction, user = await client.wait_for('reaction_add', timeout=0.8)
                    evlist.append((reaction, user))
                except asyncio.TimeoutError: pass
                #time.sleep(0.8)
            timeleft = ', 0s left\n'
            await promptmessage.edit(content=(prompt + timeleft + voters))

            outcome, summary, support = game.process_votes(evlist, yes=yesemoji, no=noemoji)
            
            if outcome: await message.channel.send('Results for game #{}\n{}\nResult: **Execute** {} ({} votes for)'.format(gameid+1, summary, target.mention, support))
            else: await message.channel.send('Results for game #{}\n{}\nResult: **Spare** {}'.format(gameid+1, summary, target.mention))

        ### ADDVOTE ###
        elif noprefix.startswith('addvote'):
            try: game = get_game(games, message.content)
            except NoGamesError:
                await message.channel.send('No games are running right now.')
                return
            except AmbiguousGameError:
                await message.channel.send('Please specify a game. Use /list to get a list of running games.\nUsage: /vote PLAYER #GAMEID')
                return
            
            if message.author != game.storyteller: 
                await message.channel.send('Error: You are not the Storyteller for this game!')
                return

            count = game.restore_deadvote(grep_mentions(message.content))
            if count: await message.channel.send('Successfully restored deadvote for {} players'.format(count))
            else: await message.channel.send('Failed to restore any deadvotes'.format(count))

        ### ADDVOTE ###
        elif noprefix.startswith('rmvote'):
            try: game = get_game(games, message.content)
            except NoGamesError:
                await message.channel.send('No games are running right now.')
                return
            except AmbiguousGameError:
                await message.channel.send('Please specify a game. Use /list to get a list of running games.\nUsage: /vote PLAYER #GAMEID')
                return
            
            if message.author != game.storyteller: 
                await message.channel.send('Error: You are not the Storyteller for this game!')
                return

            count = game.remove_deadvote(grep_mentions(message.content))
            if count: await message.channel.send('Successfully removed deadvote for {} players'.format(count))
            else: await message.channel.send('Failed to remove any deadvotes'.format(count))

    print('Started!')
    client.run(token)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('config')
    args = parser.parse_args()

    with open(args.config) as fh: config = load_config(args.config)

    main(config)
