#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import clocktower
import time

class TestUser(object):
	def __init__(self, name):
		self.display_name = name
		self.name = name
		self.id = hash(name)



def clockify(game, outfile):
	fig = plt.figure()
	fig.set_tight_layout(True)
	ax = fig.gca(projection='polar')

	curtime = time.gmtime()
	hour = ((curtime.tm_hour % 12) + (curtime.tm_min / 60))/12 * 2 * np.pi
	hour = np.pi/2 - hour
	ax.plot([0,hour], [0,1.0], lw=10, color='#333', zorder=-1, alpha=0.1)

	minute = (curtime.tm_min + curtime.tm_sec / 60)/60 * 2 * np.pi
	minute = np.pi/2 - minute
	ax.plot([0,minute], [0,1.4], lw=7, color='#333', zorder=-1, alpha=0.1)

	for i, player in enumerate(game.players):
		angle = i / len(game.players) * 2 * np.pi
		angle = np.pi/2 - angle
		radius = 1

		if game.dead[i]: color = '#ccc'
		else: color = '#fc6'

		ax.scatter(angle, radius, marker='.', s=10000*4/len(game.players), color=color, alpha=0.9)
		ax.annotate(player.display_name, (angle, radius), ha='center', va='center')

		if game.dead[i] and not game.deadvotes[i]: 
			ax.scatter(angle, radius, marker='x', s=2500*4/len(game.players), color='#f33')
	ax.set_xticks([])
	ax.set_yticks([])
	ax.set_ylim([0,1.5])

	fig.set_figwidth(3)
	fig.set_figheight(3)
	fig.savefig(outfile)
	plt.close()

if __name__ == '__main__':
	u0 = User('BigBenDroid')
	u1 = User('Usertest')
	u2 = User('Osmium')
	u3 = User('Levelping')
	u4 = User('Evil Bob')
	u5 = User('u5')
	u6 = User('u6')
	u7 = User('u7')
	u8 = User('u8')
	players = [u1,u2,u3,u4,u5,u6,u7,u8]
	game = clocktower.Game(*players[:5], storyteller=u0)
	game.dead[0] = True
	game.deadvotes[0] = False 
	game.dead[1] = True

	clockify(game, 'test.png')
